package org.apache.synapse.rest;

import java.util.HashMap;
import java.util.Map;

/**
 * (추가) 
 * @author yangwenry
 *
 */
public abstract class AbstractHandler implements Handler{

    /** A list of simple properties that would be set on the class before being used */
    protected final Map<String, Object> properties = new HashMap<String, Object>();

    public void addProperty(String name, Object value) {
        properties.put(name, value);
    }

    public Map getProperties() {
        return properties;
    }
}